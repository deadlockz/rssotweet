# RSS-o-Tweet

![logo](img/Icon.png)

This is a Atom RSS Feed Reader and and Twitter Timeline and Hashtag Reader. 
You can add many RSS-Urls, Hashtags or Usernames (timelines). Login to
Twitter and tweet text and a low resolution picture is possible, too.

Get APK from here <https://gitlab.com/deadlockz/rssotweet/raw/master/app/release/de.digisocken.rss_o_tweet.apk?inline=false>

## Features

- english only interface
- c64 ttf font
- it is BLUE in night
- newspaper style
- everyting in one list
- RSS images and twitter avatar
- landscape mode with preview on right side
- portrait mode with small preview
- search text content
- night mode
- every news is a notification
- ignore mode
- mark feeds as deleted, readed, favourite
- good blacklist (german sports, only on first open it in prefenences it gets active)
- improvement to open xml and rss ending links in Rss-o-Tweet as new source
- regex for replacing chars
- widget (only first source) with preview and without exception
- icon alternative (72x72)
- no error notifications about "ups no internet"
- Text and Image is resizeable
- Tab behavior changeable
- THE TWITTER STUFF IS AN OPTION !
- ignore retweets as an option
- activate or deactivate different sources
- login to Twitter and tweet a low resuluton (!) picture and some text.
- tweet replay and other stuff is possible in preview windows

- many more ...

## Twitter

You have to generate a Api key and Api secret on [apps.twitter.com](https://apps.twitter.com).
Please use `twittersdk://` as callback URL!

## database export and import (replace complete)

The import (.sqlite file from FileManager) is NOT a sync! it replaces the rss-feed Database complete! 

## opml import and export

- export (un)checked rss feed urls to an opml file in documents folder
- import an opml xml file via "open with ..." from a filemanger to import the *xmlUrl* attribute as new feed source

## Podcast improvement

If a feed url links ends with **.mp3**, **.ogg** or **.mp4** the App adds a play-icon.
It plays the sound and video from the internet (it does not download and cache it!!).

## Bugs

- yesss
- to test: tweets only update at first time (add api key+secret)
- not very power saving (maybe ignore mode is bad?)

## todo

- store and import preferences etc.

## License

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to [http://unlicense.org](http://unlicense.org)

## Privacy Policy

### Personal information.

Personal information is data that can be used to uniquely identify or contact a single person. I DO NOT collect, transmit, store or use any personal information while you use this app.

### Non-Personal information.

I DO NOT collect non-personal information like user's behavior:

 -  to solve App problems
 -  to show personalized ads

The google play store collect non-personal information such as the data of install (country and equipment). If you use the twitter function, take a look to the Privacy Policy of Twitter!

### Privacy Questions.

If you have any questions or concerns about my Privacy Policy or data processing, please contact me.
